# Code Compressor
 
Code Compressor allows you to take your CSS or Javascript code and minify it, removing all unnecessary characters including white space characters, new line characters, comments, and sometimes block delimiters. 

Code Compressor is a Windows 8 Modern app, available on the Windows Store.