﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace CodeCompressor
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : CodeCompressor.Common.LayoutAwarePage
    {
        /// <summary>
        /// Code Compressor itself
        /// </summary>
        private Compressor compressor;

        /// <summary>
        /// URL to the privacy policy
        /// </summary>
        private const string privacyPolicyUrl = "http://muzene.com/win8apps/privacypolicy/#codecompressor";

        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            //If we are not at first start
            if (pageState != null)
            {
                //Check if there is a value for DB in the pageState
                object obj;
                if (pageState.TryGetValue("Compressor", out obj))
                {
                    //There is, get the current compressor out of the previous state.
                    compressor = (Compressor)obj;
                }
                else
                    //Else, create a new compressor
                    compressor = new Compressor();

                //If there is a value for the code strings in the pageState, pull it out too
                if (pageState.TryGetValue("UncompressedText", out obj))
                    UncompressedText.Text = (string)obj;
                if (pageState.TryGetValue("CompressedText", out obj))
                    CompressedText.Text = (string)obj;
            }
            else
                //Else, just create a new compressor
                compressor = new Compressor();

            //Add the privacy policy to the settings pane
            SettingsPane.GetForCurrentView().CommandsRequested += MainPage_CommandsRequested;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            pageState.Add("Compressor", compressor);
            pageState.Add("UncompressedText", UncompressedText.Text);
            pageState.Add("CompressedText", CompressedText.Text);
        }

        /// <summary>
        /// Compress the text in the uncompressed box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void CompressButton_Click(object sender, RoutedEventArgs e)
        {
            //Show the loading bar
            LoadingBar.Visibility = Visibility.Visible;

            //Send the uncompressed code to the server and get the compressed back
            compressor.SetText(UncompressedText.Text);
            CompressorError error = await compressor.GetData((bool)CSSRadio.IsChecked ? CompressorSetting.CSS : CompressorSetting.Javscript);

            //Check for errors
            switch (error)
            {
                case CompressorError.Success:
                    CompressedText.Text = compressor.CompressedText;
                    ErrorBox.Visibility = Visibility.Collapsed;
                    break;
                case CompressorError.NoInternet:
                    ErrorBox.Text = "No internet connection available.";
                    ErrorBox.Visibility = Visibility.Visible;
                    break;
                case CompressorError.ServerError:
                    ErrorBox.Text = "Server access not available. Please try again later.";
                    ErrorBox.Visibility = Visibility.Visible;
                    break;
                case CompressorError.Error:
                    ErrorBox.Text = "Error with connection. Please contact codecompressor@muzene.com";
                    ErrorBox.Visibility = Visibility.Visible;
                    break;
            }

            //Hide the loading bar
            LoadingBar.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Code to run when the uncompressed box loads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void Box_Loaded(object sender, RoutedEventArgs args)
        {
            TextBox box = sender as TextBox;
            box.GotFocus += Box_GotFocus;
        }

        /// <summary>
        /// Code to run when the uncompressed box is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void Box_GotFocus(object sender, RoutedEventArgs args)
        {
            TextBox numBox = sender as TextBox;
            if (numBox.Text == "Enter Code Here")
                numBox.Text = String.Empty;
        }

        /// <summary>
        /// Add commands to the settings pane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void MainPage_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            //Clear out the current ApplicationCommands
            args.Request.ApplicationCommands.Clear();

            //Create the Privacy Policy command
            SettingsCommand privacyPref = new SettingsCommand("privacyPref", "Privacy Policy", (uiCommand) =>
            {
                LaunchPrivacyPolicy();
            });

            //Add the Privacy Policy command to the Settings pane
            args.Request.ApplicationCommands.Add(privacyPref);
        }

        /// <summary>
        /// Launch the URL to the privacy policy link
        /// </summary>
        private async void LaunchPrivacyPolicy()
        {
            //Create the link to the privacy policy and launch it
            Uri privacyLink = new Uri(privacyPolicyUrl);
            await Windows.System.Launcher.LaunchUriAsync(privacyLink);
        }
    }
}
