﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;

namespace CodeCompressor
{
    public enum CompressorError
    {
        ServerError,
        Error,
        NoInternet,
        Success
    }

    public enum CompressorSetting
    {
        CSS,
        Javscript
    }

    class Compressor
    {
        /// <summary>
        /// Uncompressed code
        /// </summary>
        private string uncompressedText;

        /// <summary>
        /// Compressed code
        /// </summary>
        public string CompressedText
        {
            get;
            private set;
        }

        /// <summary>
        /// URL for the CSS minifier
        /// </summary>
        private const string cssUrl = "http://cssminifier.com/raw";
        
        /// <summary>
        /// URL for the Javascript minifier
        /// </summary>
        private const string javascriptUrl = "http://javascript-minifier.com/raw";

        /// <summary>
        /// Create an instance of Compressor
        /// </summary>
        public Compressor()
        {
            uncompressedText = "";
            CompressedText = "";
        }

        /// <summary>
        /// Set the text to be compressed
        /// </summary>
        /// <param name="text">Text to be compressed</param>
        public void SetText(string text)
        {
            uncompressedText = text;
        }

        /// <summary>
        /// Get the compressed data from the server
        /// </summary>
        /// <param name="setting">CSS or Javascript?</param>
        /// <returns>Error from result</returns>
        public async Task<CompressorError> GetData(CompressorSetting setting)
        {
            //Check if the internet connection is available
            if (!IsInternet())
                return CompressorError.NoInternet;

            try
            {
                //Set up the HTTPClient and the message as POST
                //Set up for either Javascript or CSS
                HttpClient httpClient = new HttpClient();
                HttpRequestMessage httpContent = new HttpRequestMessage(HttpMethod.Post, (setting == CompressorSetting.CSS ? 
                                                                                          cssUrl : javascriptUrl));

                //NOTE: Your server was returning 417 Expectation failed, this is set so the request client doesn't expect 100 and continue.
                httpContent.Headers.ExpectContinue = false;

                //Set the POST data to e sent, the code to be compressed
                List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
                values.Add(new KeyValuePair<string, string>("input", uncompressedText));

                //Encode the values and set the content as it
                httpContent.Content = new FormUrlEncodedContent(values);

                //Send everything to the server
                HttpResponseMessage response = await httpClient.SendAsync(httpContent);

                //Get the response - here is the hash as string
                CompressedText = await response.Content.ReadAsStringAsync();

                return CompressorError.Success;
            }
            catch (HttpRequestException e)
            {
                return CompressorError.ServerError;
            }
            catch (Exception e)
            {
                return CompressorError.Error;
            }
        }

        /// <summary>
        /// Check if we have access to the internet
        /// </summary>
        /// <returns>If we have internet connection</returns>
        public static bool IsInternet()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }
    }
}
